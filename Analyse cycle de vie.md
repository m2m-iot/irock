Jérémy Sanchez
Simon Troussier
Maxence Bleton--Giordano

# Irock

Le système IRock est un dispositif à placer dans un environnement montagneux afin de détecter les glissements de terrains. Les informations sont remontées à l'aide du protocole LORA et enregistré dans un base de données de LORA CAMPUS IOT. Le système placé sur le terrain est constitué d'un capteur SICONIA fabriqué par SAGECOM. Le capteur est placé dans du polystyrène pour résister aux chocs.

# Analyse du cycle de vie
### Matériaux utilisés: 
 * Le composant principal est un capteur SICONIA fabriqué par SAGECOM.
 * Polystyrène placé autour du capteur pour sa protection
 
### Autonomnie: 
L'automnie estimé de l'IRock est de 2 ans. En effet, le dispositif transmet uniquement un heartbeat par jour et une information de glissement et de déplacement quand l'IRock detecte un mouvement.

### Impacte sur l'environnement:
Le seul impacte envisageable pour l'environnement est si nous perdions l'IRock car une batterie est présente dans le dispositif.

# Stratégie nationale de transition écologique vers un développement durable
Ce dispositif permet de garantir une surveillance des terrains et de routes dans des régions reculées afin de garantir une bonne desserte de ces derniers. (Axe 1 & Axe 3).
Sensibiliser la jeunesse aux dangers de la montagne et au mouvement des falaises que ne nous ne pouvons maitriser totalement.(Axe 7).
Pouvoir prévenir les gros glissements de terrains pouvant impacter des sites industrielles ce qui engendrerait un impact environnemental et économique sur la région.(exemple Séchilienne).(Axe 2,6,8,9)

Pas d'axe 4,5, trouvés.
