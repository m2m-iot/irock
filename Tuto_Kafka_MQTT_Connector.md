# Kafka Mqtt Connector
## Tutorial for create an kafka mqtt connector

### Installation :
- java :
``` bash 
sudo apt install openjdk-11-jre-headless
```
- Confluent :
``` bash 
wget -qO - https://packages.confluent.io/deb/5.4/archive.key |sudo apt-key add 
sudo add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/5.4 stable main"
sudo apt-get update && sudo apt-get install confluent-platform-2.12
sudo confluent-hub install confluentinc/kafka-connect-mqtt:1.2.3
```
- Confluent CLI :
``` bash 
curl -L --http1.1 https://cnfl.io/cli | sh -s -- -b /<path-to-cli>
```
Command “confluent” will refer to the script downloaded here
- Mosquitto
``` bash 
sudo apt-get install -y mosquitto-clients
sudo apt install mosquitto
```

### Local broker :
#### SetUp :
- Launch Mosquitto server (as background task or in its own terminal) :
``` bash
mosquitto
```

- Launch Confluent :
``` bash
confuent local start
```
- Send Connector config :
``` bash
curl -s -X POST -H 'Content-Type: application/json' http://localhost:8083/connectors -d '{
    "name" : "mqtt-source",
"config" : {
    "connector.class" : "io.confluent.connect.mqtt.MqttSourceConnector",
    "tasks.max" : "1",
    "mqtt.server.uri" : "tcp://127.0.0.1:1883",
    "mqtt.topics" : "temperature",
    "kafka.topic" : "mqtt.temperature",
    "confluent.topic.bootstrap.servers": "localhost:9092",
    "confluent.topic.replication.factor": "1",
    "confluent.license":""
    }
}'
```
Our example topic is named temperature. Data will be send manually.
- Create kafka Topics :
``` bash
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic mqtt.temperature
```
#### Launch :
- Kafka consumer (as background task or in its own terminal) : 
``` bash
kafka-console-consumer --bootstrap-server localhost:9092 --topic mqtt.temperature --property print.key=true --from-beginning
```
- Send Data (example) :
``` bash
mosquitto_pub -h 127.0.0.1 -p 1883 -t temperature -q 2 -m "99999,2.10#"
mosquitto_pub -h 127.0.0.1 -p 1883 -t temperature -q 2 -m "fraud"
mosquitto_pub -h 127.0.0.1 -p 1883 -t temperature -q 2 -m "99999,2.10# 2.13# 2.19# 2.28# 2.44# 2.62# 2.80# 3.04# 3.36# 3.69# 3.97# 4.24# 4.53#4.80# 5.02# 5.21# 5.40# 5.57# 5.71# 5.79# 5.86# 5.92# 5.98# 6.02# 6.06# 6.08# 6.14# 6.18# 6.22# 6.27#6.32# 6.35# 6.38# 6.45# 6.49# 6.53# 6.57# 6.64# 6.70# 6.73# 6.78# 6.83# 6.88# 6.92# 6.94# 6.98# 7.01#7.03# 7.05# 7.06# 7.07# 7.08# 7.06# 7.04# 7.03# 6.99# 6.94# 6.88# 6.83# 6.77# 6.69# 6.60# 6.53# 6.45#6.36# 6.27# 6.19# 6.11# 6.03# 5.94# 5.88# 5.81# 5.75# 5.68# 5.62# 5.61# 5.54# 5.49# 5.45# 5.42# 5.38#5.34# 5.31# 5.30# 5.29# 5.26# 5.23# 5.23# 5.22# 5.20# 5.19# 5.18# 5.19# 5.17# 5.15# 5.14# 5.17# 5.16#5.15# 5.15# 5.15# 5.14# 5.14# 5.14# 5.15# 5.14# 5.14# 5.13# 5.15# 5.15# 5.15# 5.14# 5.16# 5.15# 5.15#5.14# 5.14# 5.15# 5.15# 5.14# 5.13# 5.14# 5.14# 5.11# 5.12# 5.12# 5.12# 5.09# 5.09# 5.09# 5.10# 5.08# 5.08# 5.08# 5.08# 5.06# 5.05# 5.06# 5.07# 5.05# 5.03# 5.03# 5.04# 5.03# 5.01# 5.01# 5.02# 5.01# 5.01#5.00# 5.00# 5.02# 5.01# 4.98# 5.00# 5.00# 5.00# 4.99# 5.00# 5.01# 5.02# 5.01# 5.03# 5.03# 5.02# 5.02#5.04# 5.04# 5.04# 5.02# 5.02# 5.01# 4.99# 4.98# 4.96# 4.96# 4.96# 4.94# 4.93# 4.93# 4.93# 4.93# 4.93# 5.02# 5.27# 5.80# 5.94# 5.58# 5.39# 5.32# 5.25# 5.21# 5.13# 4.97# 4.71# 4.39# 4.05# 3.69# 3.32# 3.05#2.99# 2.74# 2.61# 2.47# 2.35# 2.26# 2.20# 2.15# 2.10# 2.08"
```
#### Set Down :
``` bash
confluent local destroy
```
### Campus IOT broker (Doesn’t work):
### SetUp :
- Launch Confluent :
``` bash
confuent local start
```
- Send Connector config :
``` bash
curl -s -X POST -H 'Content-Type: application/json' http://localhost:8083/connectors -d '{
    "name" : "campus-iot",
    "config" : {
        "connector.class" : "io.confluent.connect.mqtt.MqttSourceConnector",
"tasks.max" : "1",
"mqtt.server.uri" : "tcp://lora.campusiot.imag.fr:1883",
"mqtt.topics" : "application/#",
"mqtt.username" : "org-15",
"mqtt.password" : "eHUmevRzhXM93vD3FAIxtsg7",
"kafka.topic" : "mqtt.campusiot",
"confluent.topic.bootstrap.servers": "localhost:9092",
"confluent.topic.replication.factor": "1",
"confluent.license":""
   }
}'
```
- Create kafka Topics :
``` bash
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic mqtt.campusiot
```
#### Launch :
- Kafka consumer (as background task or in its own terminal) : 
``` bash
kafka-console-consumer --bootstrap-server localhost:9092 --topic mqtt.campusiot --property print.key=true --from-beginning
```
#### Set Down :
``` bash
confluent local destroy
```
