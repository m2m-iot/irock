Jérémy Sanchez
Simon Troussier
Maxence Bleton--Giordano

# IRock

# Security and Privacy

Le dispositif IRock ne stocke aucune donnée localement. Il serait intéressant de chiffrer les données transmises par le réseau LORA même si ces données ne présentent aucune donnée sensibles. 

Le risque majeur du système IRock serait un acte malveillant venant à déplacer l'IRock ce qui pourrait être interprété comme un glissement de terrain et donc tromper l'analyse des données. De plus un vol de l'IRock engendrerait une perte d'information sur le terrain et donc un potentiel manque de prévention de glissement de terrain.

# RGPD

Nous ne pouvons pas répondre à cette question car nous n'avons pas travaillé principalement sur ce projet.
On peut supposer que le système envoie peu de données et que celle n'ont pas de caractère personnelle. Il serait uniquement nécessaire de sécuriser les données lors de l'envoie, la réception et le stockage des données. De plus il faudrait respecter les normes RGPD concernant la plateforme permettant les accès aux données (compte etc), leur manipulation et traitement.


